import time

import numpy as np
from matplotlib import pyplot as plt
from mpmath import mp, mpf
from sympy import symbols, Poly


def timer(func):
    """декоратор выводящий время работы функции (взят из интернета)
    :param func:
    :return:
    """
    def inner(*args, **kwargs):
        t1 = time.time()
        f = func(*args, **kwargs)
        t2 = time.time()
        print('Runtime took {0} seconds'.format(t2 - t1))
        return f

    return inner


def bound_condition(coeffs, point, value):
    """
    функция трансформирующая ряд с учетом граничного условия
    :param coeffs: к-ты ряда
    :param point: точка наложения граничного условия
    :param value: значение граничного условия
    :return: список с новыми к-тами ряда
    """
    x = symbols('x')
    f = Poly(coeffs[::-1], x)
    ff = ((f - value) / (point - x)).series(x, 0, len(coeffs))
    return [ff.coeff(x, i) for i in range(len(coeffs))]


def bt(coeffs, b):
    """
    преобразование Бореля(-Лероя) для ряда
    :param coeffs: к-ты ряда
    :param b: параметр b=b0+3/2 (b0 параметр АВП)
    :return: к-ты ряда после преобразования Бореля
    """
    res = []
    for i in range(len(coeffs)):
        res.append(coeffs[i] / mp.gamma(b + i + 1))
    return res


def cm(bcoeffs, a, v):
    """
    построение к-тов ряда после конформного маппинга с учетом асимптотики 'сильной связи'
    :param bcoeffs: к-ты преобразованного по Борелю ряда
    :param a: параметр асимптотики высоких порядков (АВП)
    :param v: параметр сильной связи
    :return: к-ты ряда по конформной переменной (x/w)^v*(sum c_n * w^n)
    """
    s = 0
    w = symbols('w')
    t = mpf(4) * w / a / (mpf(1) - w) ** (2)
    for i in range(len(bcoeffs)):
        s += bcoeffs[i] * t ** (i)
    s = s * (w / t) ** v

    ss = s.series(w, 0, len(bcoeffs)).removeO()

    return [ss.coeff(w, i) for i in range(len(bcoeffs))]


def integrate(coeffs, point, a, b, v):
    """
    Обратное преобразование Бореля (интеграл выполняется по переменной w, вместо t -- причина:
    по w пределы интегрирования конечны)
    :param coeffs: к-ты ряда по конформной переменной
    :param point: точка в которой вычисляется преобразование Бореля
    :param a: параметр АВП
    :param b: параметр преобразования Бореля-Лероя
    :param v: параметр сильной связи
    :return: пересуммированный ряд
    """
    t = lambda w: mpf(4) * w / a / (mpf(1) - w) ** mpf(2)

    integrand = lambda w: \
        sum([coeffs[i] * w ** i for i in range(len(coeffs))]) * \
        (t(w) / w) ** v * \
        mp.exp(-t(w) / point) / point * \
        (t(w) / point) ** b * \
        mpf(4) * (w + mpf(1)) / a / (mpf(1) - w) ** mpf(3)
    res = mp.quad(integrand, (0, 1))
    return res


def w_series(coeffs, point=mpf(2), a=mpf(1) / 3, b=mpf(5)):
    """
    построение ряда по конформной переменной из исходного ряда для произвольных f2 и v.
    нулевой порядок исключается из процедуры.
    :param coeffs: к-ты изначального ряда
    :param point: точка пересуммирования
    :param a: параметр АВП
    :param b: параметр Бореля-Лероя
    :return: (c, f2, v)
    c -- к-ты ряда по конформной переменной
    f2,v - переменные sympy от которых зависят к-ты c
    """
    f2 = symbols('f2')

    bcl = bt(bound_condition(coeffs, point, f2), b)
    v = symbols('v')

    bcl_ = [mpf(0)] + bcl[1:]

    c = cm(bcl_, a, v)

    return c, f2, v


def inv_borel(coeffs, point, a, b, v, f2, bound_point):
    """
    обратное преобразование ряда с учетом, преобразования "граничных условий"
    :param coeffs: к-ты ряда по конформной переменной
    :param point: точка пересуммирования
    :param a: параметр АВП
    :param b: параметр Бореля-Лероя
    :param v: параметр сильной связи
    :param f2: граничное условие
    :param bound_point: точка наложения граничного условия
    :return: пересуммированное значение
    """
    return f2 + (bound_point - point) * (-f2 / mpf(2) + integrate(coeffs, point, a, b, v))


@timer
def borel(coeffs, point, a, b, v_val, f2_val, bound_point):
    """
    вычисление пересуммированного по Борелю значения для заданных v и f2
    :param coeffs: к-ты изначального ряда
    :param point: точка пересуммирования
    :param a: параметр АВП
    :param b: параметр Бореля-Лероя
    :param v_val: значение v
    :param f2_val: Значение f2
    :param bound_point: точка наложения граничного условия
    :return: пересуммированное значение
    """
    c, f2, v = w_series(coeffs)

    c1 = [mpf(c[i].subs(v, v_val).subs(f2, f2_val)) for i in range(len(c))]

    return inv_borel(c1, point, a, b, v_val, f2_val, bound_point)


@timer
def borel_loops(coeffs, loops, point, a, b, v_val, f2_val, bound_point):
    """
    вычисление пересуммированного по Борелю значения для заданных v и f2 для заданного набора петель.
    причина выделения в отдельную функцию -- использование borel(..) для каждой заданной петли будет проводить
    символьные вычисления к-тов ряда (долго), в то время как данная функция делает это один раз для всех.
    :param coeffs: к-ты изначального ряда
    :param loops: список петель для которых пересуммировать
    :param point: точка пересуммирования
    :param a: параметр АВП
    :param b: параметр Бореля-Лероя
    :param v_val: значение v
    :param f2_val: значение f2
    :param bound_point: точка наложения граничного условия
    :return: список пересуммированных значений
    """
    c, f2, v = w_series(coeffs)

    c1 = [mpf(c[i].subs(v, v_val).subs(f2, f2_val)) for i in range(len(c))]

    return [inv_borel(c1[:l + 1], point, a, b, v_val, f2_val, bound_point) for l in loops]


@timer
def borel_veer(coeffs, loops, point, a, b, v_list, f2_val, bound_point):
    """
    построение веера с заданным списокм набора петель и значений v
    :param coeffs: к-ты изначального ряда
    :param loops: список петель
    :param point: точка пересуммирования
    :param a: параметр АВП
    :param b: параметр Бореля-Лероя
    :param v_list: список значений v
    :param f2_val: значение f2
    :param bound_point: точка наложения граничного условия
    :return: список "borel_loops"
    """
    c, f2, v = w_series(coeffs)

    c0 = [(c[i].subs(f2, f2_val)) for i in range(len(c))]
    res = list()
    for v_ in v_list:
        c1 = [mpf(c0[i].subs(v, mpf(v_))) for i in range(len(c))]
        res.append([inv_borel(c1[:l + 1], point, a, b, mpf(v_), f2_val, bound_point) for l in loops])

    return res


def integrate_for_Q(coeffs1, coeffs2, point, a, b, v):
    """
    обратное преобразование Бореля для величины Q (продифференцированный по v ряд)
    exp(-t) t^b (t/w)^v sum ( (C_n ln(t/w) + C'_n ) w^n)
    :param coeffs1: ряд по конформной переменной (mpf) (Cn)
    :param coeffs2: производная к-тов ряда по конформной переменной (mpf) (C'n)
    :param point: точка АВП
    :param b: параметр Бореля-Лероя
    :param v: значение v (необходимо, т.к. ряд не включает в себя (t/w)^v )
    :return:
    """
    t = lambda w: mpf(4) * w / a / (mpf(1) - w) ** mpf(2)
    integrand = lambda w: \
        sum([(coeffs1[i] * mp.ln(t(w) / w) + coeffs2[i]) * w ** i for i in range(len(coeffs1))]) * \
        (t(w) / w) ** v * \
        mp.exp(-t(w) / point) / point * \
        (t(w) / point) ** b * \
        mpf(4) * (w + mpf(1)) / a / (mpf(1) - w) ** mpf(3)
    res = mp.quad(integrand, (0, 1))
    return res


@timer
def density(coeffs, point, a, b, v_val, f2_val):
    """
    Вычисление Q для заданных v и f2
    :param coeffs: к-ты изначального ряда
    :param point: точка пересуммирования
    :param a: параметр АВП
    :param b: параметр Бореля-Лероя
    :param v_val: значение v
    :param f2_val: значение f2
    :return: значение Q
    """
    c, f2, v = w_series(coeffs)
    c0 = [c[i].subs(f2, f2_val) for i in range(len(c))]
    c1_0 = [mpf(c0[i].subs(v, v_val)) for i in range(len(c))]
    c1_1 = [mpf(c0[i].diff(v).subs(v, v_val)) for i in range(len(c))]
    C, A = [integrate_for_Q(c1_0[:-1], c1_1[:-1], point, a, b, v_val),
            integrate_for_Q([mpf(0)] * (len(c1_0) - 1) + [c1_0[-1]], [mpf(0)] * (len(c1_1) - 1) + [c1_1[-1]], point, a,
                            b, v_val)]
    B = A + C
    return mp.sqrt(A ** 2 + B ** 2 + C ** 2)


@timer
def Q_grid(coeffs, point, a, b, v_mg, f2_mg):
    """
    вычисление meshgrid для Q
    :param coeffs: к-ты изначального ряда
    :param point: точка пересуммирования
    :param a: параметр АВП
    :param b: параметр Бореля-Лероя
    :param v_mg: meshgrid для v
    :param f2_mg: meshgrid для f2
    :return: meshgrid для Q
    """
    c, f2, v = w_series(coeffs)
    cdiff = [c[i].diff(v) for i in range(len(c))]
    res = np.zeros_like(v_mg)
    for i in range(len(res)):
        print(i, (len(res)))
        for j in range(len(res[i])):
            f2_val = f2_mg[i][j]
            v_val = v_mg[i][j]
            c1_0 = [mpf(c[i].subs(v, v_val).subs(f2, f2_val)) for i in range(len(c))]
            c1_1 = [mpf(cdiff[i].subs(v, v_val).subs(f2, f2_val)) for i in range(len(c))]
            C, A = [integrate_for_Q(c1_0[:-1], c1_1[:-1], point, a, b, v_val),
                    integrate_for_Q([mpf(0)] * (len(c1_0) - 1) + [c1_0[-1]], [mpf(0)] * (len(c1_1) - 1) + [c1_1[-1]],
                                    point, a,
                                    b, v_val)]
            B = A + C
            res[i][j] = mp.sqrt(A ** 2 + B ** 2 + C ** 2)

    return res

# можно задать точность чисел в mpmath, иногда имеет смысл увеличить точность
# и проверить, что от этого ничего не зависит

# mp.dps = 20

f_coeff_list = list(map(mpf, [0, 0, 0.0185185, 0.01869, -0.00832875, 0.0256567375, -0.08127264062, 0.314749]))

point = mpf(1)
a = mpf(1) / mpf(3)
b = mpf(5)
bound_point = mpf(2)
print(borel(f_coeff_list[:-1], point, a, b, mpf(2.2), mpf(0.2), bound_point))

print()

print(borel_loops(f_coeff_list, [3, 4, 5, 6], point, a, b, mpf(2.2), mpf(0.2), bound_point))

print(borel_loops(f_coeff_list, [3, 4, 5, 6], point, a, b, mpf(1.3), mpf(0.21), bound_point))

if False:
    """
    построение веера (отключено), для включения изменить на True
    """
    t = time.time()
    y_graph_borel = list()
    v_list = [1.3, 1.6, 1.9, 2.2, 2.5, 2.8]
    y_graph_borel = borel_veer(f_coeff_list, [3, 4, 5, 6], point, a, b, v_list, mpf(0.21), bound_point)
    print(time.time() - t)

    fig, ax = plt.subplots()
    ax.set_title('f(2)=0.21')
    # ax.set_ylim([0, 0.1])
    ax.set_xlim([3, 6])

    for i in range(len(v_list)):
        ax.plot([3, 4, 5, 6], y_graph_borel[i], label='%s' % v_list[i], marker='X')

    fig.set_figwidth(15)
    fig.set_figheight(10)
    ax.grid()
    ax.legend()
    plt.show()
    sys.exit()

print(density(f_coeff_list[:-1], point, a, b, mpf(2.16), mpf(0.206)))

#параметры meshgrid
f2s, f2e, f2st = 0.204, 0.208, 40
vs, ve, vst = 2.14, 2.17, 40

f2d = (f2e - f2s) / (f2st - 1)
vd = (ve - vs) / (vst - 1)

# параметры подобраны чтобы давать meshgrid аналогичный тому что дает mgrid (mgrid не работает с mpf)
x_f2, y_v = np.meshgrid(mp.arange(f2s, f2e + f2d, f2d), mp.arange(vs, ve + vd, vd))
# print(x_f2)
# print(y_v)

grid = Q_grid(f_coeff_list[:-1], point, a, b, y_v, x_f2)

minimum = np.amin(grid)
coords = list(zip(*np.where(#т
# f2s, f2e, f2st = 0.1, 0.3, 3
# vs, ve, vst = 3, 5, 3
grid == minimum)))

print(minimum, coords)
# теоретически минимумов может быть несколько, мы в это не верим, но на всякий случай
# выводим значения f2 и v для всех
# в качестве минимума для графика мы берем **последний** минимум в списке (в нашем случае он же и первый)
#

for i1, i2 in coords:
    print(x_f2[i1][i2], y_v[i1, i2])
f2_min, v_min = x_f2[i1][i2], y_v[i1, i2]

# print(grid)

levels_cont2 = [4 * minimum]  # изолинию погршености определяем как 4*минимум

fig, ax = plt.subplots()
cont = ax.contourf(x_f2, y_v, grid, levels=50)
cont2 = ax.contour(cont, levels=levels_cont2, colors='r')
# levels=cont.levels[1::2]

cbar = fig.colorbar(cont)

# в моей версии matplotlib следующая строчка не работает (не стал разбираться)
# cbar.add_lines(cont2)

ax.set_xlabel('f2')
ax.set_ylabel('v')
ax.scatter(f2_min, v_min, color='Gold', s=20)
fig.set_figwidth(10)
fig.set_figheight(10)
plt.show()
