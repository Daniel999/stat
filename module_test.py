from borel_functions import *

f_coeff_list = list(map(mpf, [0, 0, 0.0185185, 0.01869, -0.00832875, 0.0256567375, -0.08127264062, 0.314749]))

point = mpf(1)
a = mpf(1) / mpf(3)
b = mpf(5)
bound_point = mpf(2)
print(borel(f_coeff_list[:-1], point, a, b, mpf(2.2), mpf(0.2), bound_point))

print()

print(borel_loops(f_coeff_list, [3, 4, 5, 6], point, a, b, mpf(2.2), mpf(0.2), bound_point))

print(borel_loops(f_coeff_list, [3, 4, 5, 6], point, a, b, mpf(1.3), mpf(0.21), bound_point))

if False:
    """
    построение веера (отключено), для включения изменить на True
    """
    t = time.time()
    y_graph_borel = list()
    v_list = [1.3, 1.6, 1.9, 2.2, 2.5, 2.8]
    y_graph_borel = borel_veer(f_coeff_list, [3, 4, 5, 6], point, a, b, v_list, mpf(0.21), bound_point)
    print(time.time() - t)

    fig, ax = plt.subplots()
    ax.set_title('f(2)=0.21')
    # ax.set_ylim([0, 0.1])
    ax.set_xlim([3, 6])

    for i in range(len(v_list)):
        ax.plot([3, 4, 5, 6], y_graph_borel[i], label='%s' % v_list[i], marker='X')

    fig.set_figwidth(15)
    fig.set_figheight(10)
    ax.grid()
    ax.legend()
    plt.show()
    sys.exit()

print(density(f_coeff_list[:-1], point, a, b, mpf(2.16), mpf(0.206)))

#параметры meshgrid
f2s, f2e, f2st = 0.204, 0.208, 40
vs, ve, vst = 2.14, 2.17, 40

f2d = (f2e - f2s) / (f2st - 1)
vd = (ve - vs) / (vst - 1)

# параметры подобраны чтобы давать meshgrid аналогичный тому что дает mgrid (mgrid не работает с mpf)
x_f2, y_v = np.meshgrid(mp.arange(f2s, f2e + f2d, f2d), mp.arange(vs, ve + vd, vd))
# print(x_f2)
# print(y_v)

grid = Q_grid(f_coeff_list[:-1], point, a, b, y_v, x_f2)

minimum = np.amin(grid)
coords = list(zip(*np.where(#т
# f2s, f2e, f2st = 0.1, 0.3, 3
# vs, ve, vst = 3, 5, 3
grid == minimum)))

print(minimum, coords)
# теоретически минимумов может быть несколько, мы в это не верим, но на всякий случай
# выводим значения f2 и v для всех
# в качестве минимума для графика мы берем **последний** минимум в списке (в нашем случае он же и первый)
#

for i1, i2 in coords:
    print(x_f2[i1][i2], y_v[i1, i2])
f2_min, v_min = x_f2[i1][i2], y_v[i1, i2]

# print(grid)

levels_cont2 = [4 * minimum]  # изолинию погршености определяем как 4*минимум

fig, ax = plt.subplots()
cont = ax.contourf(x_f2, y_v, grid, levels=50)
cont2 = ax.contour(cont, levels=levels_cont2, colors='r')
# levels=cont.levels[1::2]

cbar = fig.colorbar(cont)

# в моей версии matplotlib следующая строчка не работает (не стал разбираться)
# cbar.add_lines(cont2)

ax.set_xlabel('f2')
ax.set_ylabel('v')
ax.scatter(f2_min, v_min, color='Gold', s=20)
fig.set_figwidth(10)
fig.set_figheight(10)
plt.show()
