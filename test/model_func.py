import time

import numpy as np
from matplotlib import pyplot as plt
from mpmath import mp, mpf
from sympy import symbols, Poly

from borel_functions_new import *

mp.dps = 15

point = mpf(0.25)
a = mpf(4)
b = mpf(3) / mpf(2)
bound_point = mpf(0.5)

f_coeff_list = list(map(mpf, [0.5, -1.5, 12.0, -148.5, 2448.0, -50139.0]))
#f_coeff_list = list(map(mpf, [0.5, -1.5, 12.0, -148.5, 2448.0, -50139.0, 1225152.0,-34766698.5]))

print(borel(f_coeff_list, point, a, b, mpf(-0.5), mpf(0.2896), bound_point))