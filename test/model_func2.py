from borel_cm_fbc import borel_functions as bf
from mpmath import mp, mpf
mp.dps = 15

point = mpf(0.25)
a = mpf(4)
b = mpf(3) / mpf(2)
bound_point = mpf(0.5)

f_coeff_list = list(map(mpf, [0.5, -1.5, 12.0, -148.5, 2448.0, -50139.0]))
# f_coeff_list = list(map(mpf, [0, -1.5, 12.0, -148.5, 2448.0, -50139.0]))
#f_coeff_list = list(map(mpf, [0.5, -1.5, 12.0, -148.5, 2448.0, -50139.0, 1225152.0,-34766698.5]))

print(bf.borel(f_coeff_list, point, a, b, mpf(-0.5), mpf(0.2896), bound_point))

v=mpf(0)
v=mpf(-0.5)

bs = bf.bt(f_coeff_list, b)
cs = bf.cm(bs, a, v)

print(bs)
print(cs)

res = bf.inv_bt(cs,point,a,b,v)
print(res)

bcs = bf.bound_condition(f_coeff_list,bound_point,mpf(0.2896))
bs  = bf.bt(bcs, 0)
print(bcs)
print(bs)