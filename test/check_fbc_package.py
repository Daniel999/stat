from mpmath import mp, mpf

from borel_cm_fbc import borel, borel_loops
from phi4exponents import exp_eta, exp_1nu, exp_omega, exp_gmz, utils

mp.dps = 15

f_coeff_list = list(map(mpf, [0, 0, 0.0185185, 0.01869, -0.00832875, 0.0256567375, -0.08127264062, 0.314749]))

print(f_coeff_list)

exponent = utils.eps2toeps1(exp_eta(1))
# exponent = utils.eps2toeps1(exp_gmz(1))
# exponent = utils.eps2toeps1(exp_omega(1))

print(exponent)

point = mpf(1)
a = mpf(1) / mpf(3)
b = mpf(5)
bound_point = mpf(2)
print(borel(exponent[:-1], point, a, b, mpf(2.2), mpf(0.2), bound_point))

print()

print(borel_loops(exponent, [3, 4, 5, 6], point, a, b, mpf(2.2), mpf(0.2), bound_point))

print(borel_loops(exponent, [3, 4, 5, 6], point, a, b, mpf(1.3), mpf(0.21), bound_point))
