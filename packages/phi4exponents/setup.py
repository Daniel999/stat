#!/usr/bin/python
# -*- coding: utf8
from distutils.core import setup

setup(
    name='FBC Resummation',
    version='0.0.1',
    author='M. Kompaniets',
    author_email='',
    packages=['phi4exponents'],
    # url='http://pypi.python.org/pypi/xxx/',
    license='LICENSE.txt',
    description='Series for exponents of the phi4 model up to 7 loops (based on O.Schnetz paper Phys. Rev. D 97, 085018)',
    long_description=open('README.txt').read(),
)
