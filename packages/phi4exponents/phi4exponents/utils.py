from mpmath import mpf


def eps2toeps1(coeffs):
    return [coeffs[i]/mpf(2)**i for i in range(len(coeffs))]
