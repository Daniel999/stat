#!/usr/bin/python
# -*- coding: utf8
from distutils.core import setup

setup(
    name='FBC Resummation',
    version='0.0.1',
    author='D. Evdokimov, M. Kompaniets, D. Zakharov',
    author_email='',
    packages=['borel_cm_fbc'],
    # url='http://pypi.python.org/pypi/xxx/',
    license='LICENSE.txt',
    description='Free boundary condition resummation for asymptotic series',
    long_description=open('README.txt').read(),
)
