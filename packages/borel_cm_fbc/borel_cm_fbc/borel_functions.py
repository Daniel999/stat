import numpy as np
from mpmath import mp, mpf
from sympy import symbols, Poly


def bound_condition(coeffs, point, value):
    """
    функция трансформирующая ряд с учетом граничного условия
    :param coeffs: к-ты ряда
    :param point: точка наложения граничного условия
    :param value: значение граничного условия
    :return: список с новыми к-тами ряда
    """
    x = symbols('x')
    f = Poly(coeffs[::-1], x)
    ff = ((f - value) / (point - x)).series(x, 0, len(coeffs))
    return [ff.coeff(x, i) for i in range(len(coeffs))]


def bt(coeffs, b):
    """
    преобразование Бореля(-Лероя) для ряда
    :param coeffs: к-ты ряда
    :param b: параметр b=b0+3/2 (b0 параметр АВП)
    :return: к-ты ряда после преобразования Бореля
    """
    res = []
    for i in range(len(coeffs)):
        res.append(coeffs[i] / mp.gamma(b + i + 1))
    return res


def cm(bcoeffs, a, v):
    """
    построение к-тов ряда после конформного маппинга с учетом асимптотики 'сильной связи'
    :param bcoeffs: к-ты преобразованного по Борелю ряда
    :param a: параметр асимптотики высоких порядков (АВП)
    :param v: параметр сильной связи
    :return: к-ты ряда по конформной переменной (x/w)^v*(sum c_n * w^n)
    """
    s = 0
    w = symbols('w')
    t = mpf(4) * w / a / (mpf(1) - w) ** (2)
    for i in range(len(bcoeffs)):
        s += bcoeffs[i] * t ** (i)
    s = s * (w / t) ** v

    ss = s.series(w, 0, len(bcoeffs)).removeO()

    return [ss.coeff(w, i) for i in range(len(bcoeffs))]


def inv_bt(coeffs, point, a, b, v):
    """
    Обратное преобразование Бореля (интеграл выполняется по переменной w, вместо t -- причина:
    по w пределы интегрирования конечны)
    :param coeffs: к-ты ряда по конформной переменной
    :param point: точка в которой вычисляется преобразование Бореля
    :param a: параметр АВП
    :param b: параметр преобразования Бореля-Лероя
    :param v: параметр сильной связи
    :return: пересуммированный ряд
    """
    t = lambda w: mpf(4) * w / a / (mpf(1) - w) ** mpf(2)

    integrand = lambda w: \
        sum([coeffs[i] * w ** i for i in range(len(coeffs))]) * \
        (t(w) / w) ** v * \
        mp.exp(-t(w) / point) / point * \
        (t(w) / point) ** b * \
        mpf(4) * (w + mpf(1)) / a / (mpf(1) - w) ** mpf(3)
    res = mp.quad(integrand, (0, 1))
    return res


def w_series(coeffs, bound_point, a, b):
    """
    построение ряда по конформной переменной из исходного ряда для произвольных f2 и v.
    нулевой порядок исключается из процедуры.
    :param coeffs: к-ты изначального ряда
    :param point: точка пересуммирования
    :param a: параметр АВП
    :param b: параметр Бореля-Лероя
    :return: (c, f2, v)
    c -- к-ты ряда по конформной переменной
    f2,v - переменные sympy от которых зависят к-ты c
    """
    f2 = symbols('f2')

    bc = bound_condition(coeffs, bound_point, f2)
    bcl = bt(bc, b)
    bcl_ = [mpf(0)] + bcl[1:]
    v = symbols('v')

    c = cm(bcl_, a, v)

    return c, f2, v, bc[0]


def inv_bc(value, point, f2, bound_point):
    """
    обратное для bound_condition преобразование
    :param value: пересуммированое значение для ряда преобразованного bound_condition
    :param point: точка пересуммирование
    :param f2: граничное условие
    :param bound_point: точка наложения граничного условия
    :return: преобразованное значение
    """
    return f2 + (bound_point - point) * ( value)


def borel(coeffs, point, a, b, v_val, f2_val, bound_point):
    """
    вычисление пересуммированного по Борелю значения для заданных v и f2
    :param coeffs: к-ты изначального ряда
    :param point: точка пересуммирования
    :param a: параметр АВП
    :param b: параметр Бореля-Лероя
    :param v_val: значение v
    :param f2_val: Значение f2
    :param bound_point: точка наложения граничного условия
    :return: пересуммированное значение
    """
    c, f2, v, c0 = w_series(coeffs, bound_point=bound_point, a=a, b=b)

    c1 = [mpf(c[i].subs(v, v_val).subs(f2, f2_val)) for i in range(len(c))]

    return inv_bc(inv_bt(c1, point, a, b, v_val)+c0.subs(f2, f2_val), point, f2_val, bound_point)


def borel_loops(coeffs, loops, point, a, b, v_val, f2_val, bound_point):
    """
    вычисление пересуммированного по Борелю значения для заданных v и f2 для заданного набора петель.
    причина выделения в отдельную функцию -- использование borel(..) для каждой заданной петли будет проводить
    символьные вычисления к-тов ряда (долго), в то время как данная функция делает это один раз для всех.
    :param coeffs: к-ты изначального ряда
    :param loops: список петель для которых пересуммировать
    :param point: точка пересуммирования
    :param a: параметр АВП
    :param b: параметр Бореля-Лероя
    :param v_val: значение v
    :param f2_val: значение f2
    :param bound_point: точка наложения граничного условия
    :return: список пересуммированных значений
    """
    c, f2, v, c0 = w_series(coeffs, bound_point, a, b)

    c1 = [mpf(c[i].subs(v, v_val).subs(f2, f2_val)) for i in range(len(c))]

    return [inv_bc(inv_bt(c1[:l + 1], point, a, b, v_val)+c0.subs(f2, f2_val), point, f2_val, bound_point) for l in loops]


def borel_veer(coeffs, loops, point, a, b, v_list, f2_val, bound_point):
    """
    построение веера с заданным списокм набора петель и значений v
    :param coeffs: к-ты изначального ряда
    :param loops: список петель
    :param point: точка пересуммирования
    :param a: параметр АВП
    :param b: параметр Бореля-Лероя
    :param v_list: список значений v
    :param f2_val: значение f2
    :param bound_point: точка наложения граничного условия
    :return: список "borel_loops"
    """
    c, f2, v, c0 = w_series(coeffs, bound_point, a, b)

    c1_ = [(c[i].subs(f2, f2_val)) for i in range(len(c))]
    res = list()
    for v_ in v_list:
        c1 = [mpf(c1_[i].subs(v, mpf(v_))) for i in range(len(c))]
        res.append([inv_bc(inv_bt(c1[:l + 1], point, a, b, mpf(v_))+c0.subs(f2, f2_val), point, f2_val, bound_point) for l in loops])

    return res


def integrate_for_Q(coeffs1, coeffs2, point, a, b, v):
    """
    обратное преобразование Бореля для величины Q (продифференцированный по v ряд)
    exp(-t) t^b (t/w)^v sum ( (C_n ln(t/w) + C'_n ) w^n)
    :param coeffs1: ряд по конформной переменной (mpf) (Cn)
    :param coeffs2: производная к-тов ряда по конформной переменной (mpf) (C'n)
    :param point: точка АВП
    :param b: параметр Бореля-Лероя
    :param v: значение v (необходимо, т.к. ряд не включает в себя (t/w)^v )
    :return:
    """
    t = lambda w: mpf(4) * w / a / (mpf(1) - w) ** mpf(2)
    integrand = lambda w: \
        sum([(coeffs1[i] * mp.ln(t(w) / w) + coeffs2[i]) * w ** i for i in range(len(coeffs1))]) * \
        (t(w) / w) ** v * \
        mp.exp(-t(w) / point) / point * \
        (t(w) / point) ** b * \
        mpf(4) * (w + mpf(1)) / a / (mpf(1) - w) ** mpf(3)
    res = mp.quad(integrand, (0, 1))
    return res


def density(coeffs, point, a, b, v_val, f2_val, bound_point):
    """
    Вычисление Q для заданных v и f2
    :param coeffs: к-ты изначального ряда
    :param point: точка пересуммирования
    :param a: параметр АВП
    :param b: параметр Бореля-Лероя
    :param v_val: значение v
    :param f2_val: значение f2
    :return: значение Q
    """
    c, f2, v, c0 = w_series(coeffs, bound_point, a, b)
    c0 = [c[i].subs(f2, f2_val) for i in range(len(c))]
    c1_0 = [mpf(c0[i].subs(v, v_val)) for i in range(len(c))]
    c1_1 = [mpf(c0[i].diff(v).subs(v, v_val)) for i in range(len(c))]
    C, A = [integrate_for_Q(c1_0[:-1], c1_1[:-1], point, a, b, v_val),
            integrate_for_Q([mpf(0)] * (len(c1_0) - 1) + [c1_0[-1]], [mpf(0)] * (len(c1_1) - 1) + [c1_1[-1]], point, a,
                            b, v_val)]
    B = A + C
    return mp.sqrt(A ** 2 + B ** 2 + C ** 2)


def Q_grid(coeffs, point, a, b, v_mg, f2_mg, bound_point):
    """
    вычисление meshgrid для Q
    :param coeffs: к-ты изначального ряда
    :param point: точка пересуммирования
    :param a: параметр АВП
    :param b: параметр Бореля-Лероя
    :param v_mg: meshgrid для v
    :param f2_mg: meshgrid для f2
    :return: meshgrid для Q
    """
    c, f2, v, c0 = w_series(coeffs, bound_point, a, b)
    cdiff = [c[i].diff(v) for i in range(len(c))]
    res = np.zeros_like(v_mg)
    for i in range(len(res)):
        print(i, (len(res)))
        for j in range(len(res[i])):
            f2_val = f2_mg[i][j]
            v_val = v_mg[i][j]
            c1_0 = [mpf(c[i].subs(v, v_val).subs(f2, f2_val)) for i in range(len(c))]
            c1_1 = [mpf(cdiff[i].subs(v, v_val).subs(f2, f2_val)) for i in range(len(c))]
            C, A = [integrate_for_Q(c1_0[:-1], c1_1[:-1], point, a, b, v_val),
                    integrate_for_Q([mpf(0)] * (len(c1_0) - 1) + [c1_0[-1]], [mpf(0)] * (len(c1_1) - 1) + [c1_1[-1]],
                                    point, a,
                                    b, v_val)]
            B = A + C
            res[i][j] = mp.sqrt(A ** 2 + B ** 2 + C ** 2)

    return res


def Q2_grid(coeffs, point, a, b, v_mg, f2_mg, bound_point):
    """
    вычисление meshgrid для Q
    :param coeffs: к-ты изначального ряда
    :param point: точка пересуммирования
    :param a: параметр АВП
    :param b: параметр Бореля-Лероя
    :param v_mg: meshgrid для v
    :param f2_mg: meshgrid для f2
    :return: meshgrid для Q
    """
    c, f2, v, c0 = w_series(coeffs, bound_point, a, b)
    print(len(coeffs),len(c))
    cdiff = [c[i].diff(v) for i in range(len(c))]
    res = np.zeros_like(v_mg)
    for i in range(len(res)):
        print(i, (len(res)))
        for j in range(len(res[i])):
            f2_val = f2_mg[i][j]
            v_val = v_mg[i][j]
            c1_0 = [mpf(c[i].subs(v, v_val).subs(f2, f2_val)) for i in range(len(c))]
            c1_1 = [mpf(cdiff[i].subs(v, v_val).subs(f2, f2_val)) for i in range(len(c))]
            C, A = [integrate_for_Q(c1_0[:-1]+[mpf(0)], c1_1[:-1]+[mpf(0)], point, a, b, v_val),
                    integrate_for_Q([mpf(0)] * (len(c1_0) - 1) + [c1_0[-1]], [mpf(0)] * (len(c1_1) - 1) + [c1_1[-1]],
                                    point, a,
                                    b, v_val)]
            B = A + C
            D = inv_bt([mpf(0)] * (len(c1_0) - 1) + [c1_0[-1]], point, a, b, v_val)
            res[i][j] = mp.sqrt(A ** 2 + B ** 2 + D ** 2)

    return res


def Q3_grid(coeffs, point, a, b, v_mg, f2_mg, bound_point):
    """
    вычисление meshgrid для Q
    :param coeffs: к-ты изначального ряда
    :param point: точка пересуммирования
    :param a: параметр АВП
    :param b: параметр Бореля-Лероя
    :param v_mg: meshgrid для v
    :param f2_mg: meshgrid для f2
    :return: meshgrid для Q
    """
    c, f2, v = w_series(coeffs, bound_point, a, b)
    print(len(coeffs),len(c))
    cdiff = [c[i].diff(v) for i in range(len(c))]
    res = np.zeros_like(v_mg)
    for i in range(len(res)):
        print(i, (len(res)))
        for j in range(len(res[i])):
            f2_val = f2_mg[i][j]
            v_val = v_mg[i][j]
            c1_0 = [mpf(c[i].subs(v, v_val).subs(f2, f2_val)) for i in range(len(c))]
            c1_1 = [mpf(cdiff[i].subs(v, v_val).subs(f2, f2_val)) for i in range(len(c))]
            C, A = [integrate_for_Q(c1_0[:-1]+[mpf(0)], c1_1[:-1]+[mpf(0)], point, a, b, v_val),
                    integrate_for_Q([mpf(0)] * (len(c1_0) - 1) + [c1_0[-1]], [mpf(0)] * (len(c1_1) - 1) + [c1_1[-1]],
                                    point, a,
                                    b, v_val)]
            B = A + C
            D = inv_bt([mpf(0)] * (len(c1_0) - 1) + [c1_0[-1]], point, a, b, v_val)
            E = inv_bt([mpf(0)] * (len(c1_0) - 2) + [c1_0[-2]], point, a, b, v_val)
            res[i][j] = mp.sqrt(A ** 2 + B ** 2 + D ** 2+ E ** 2)

    return res

def Q4_grid(coeffs, point, a, b, v_mg, f2_mg, bound_point):
    """
    вычисление meshgrid для Q
    :param coeffs: к-ты изначального ряда
    :param point: точка пересуммирования
    :param a: параметр АВП
    :param b: параметр Бореля-Лероя
    :param v_mg: meshgrid для v
    :param f2_mg: meshgrid для f2
    :return: meshgrid для Q
    """
    c, f2, v = w_series(coeffs, bound_point, a, b)
    print(len(coeffs),len(c))
    cdiff = [c[i].diff(v) for i in range(len(c))]
    res = np.zeros_like(v_mg)
    for i in range(len(res)):
        print(i, (len(res)))
        for j in range(len(res[i])):
            f2_val = f2_mg[i][j]
            v_val = v_mg[i][j]
            c1_0 = [mpf(c[i].subs(v, v_val).subs(f2, f2_val)) for i in range(len(c))]
            c1_1 = [mpf(cdiff[i].subs(v, v_val).subs(f2, f2_val)) for i in range(len(c))]
            C, A = [integrate_for_Q(c1_0[:-1]+[mpf(0)], c1_1[:-1]+[mpf(0)], point, a, b, v_val),
                    integrate_for_Q([mpf(0)] * (len(c1_0) - 1) + [c1_0[-1]], [mpf(0)] * (len(c1_1) - 1) + [c1_1[-1]],
                                    point, a,
                                    b, v_val)]
            B = A + C
            D = inv_bt([mpf(0)] * (len(c1_0) - 1) + [c1_0[-1]], point, a, b, v_val)
            F = integrate_for_Q([mpf(0)] * (len(c1_0) - 2) + [c1_0[-2]], [mpf(0)] * (len(c1_1) - 2) + [c1_1[-2]],
                                    point, a,
                                    b, v_val)
            res[i][j] = mp.sqrt(A ** 2 + B ** 2 + D ** 2 +  F ** 2)

    return res


def density2ABD(coeffs, point, a, b, v_val, f2_val, bound_point):
    """
    Вычисление Q для заданных v и f2
    :param coeffs: к-ты изначального ряда
    :param point: точка пересуммирования
    :param a: параметр АВП
    :param b: параметр Бореля-Лероя
    :param v_val: значение v
    :param f2_val: значение f2
    :return: значение Q
    """

    c, f2, v, c0 = w_series(coeffs, bound_point, a, b)
    print(len(coeffs),len(c))
    c0 = [c[i].subs(f2, f2_val) for i in range(len(c))]
    c1_0 = [mpf(c0[i].subs(v, v_val)) for i in range(len(c))]
    c1_1 = [mpf(c0[i].diff(v).subs(v, v_val)) for i in range(len(c))]
    C, A = [integrate_for_Q(c1_0[:-1], c1_1[:-1], point, a, b, v_val),
            integrate_for_Q([mpf(0)] * (len(c1_0) - 1) + [c1_0[-1]], [mpf(0)] * (len(c1_1) - 1) + [c1_1[-1]], point, a,
                            b, v_val)]
    B = A + C
    D = inv_bt([mpf(0)] * (len(c1_0) - 1) + [c1_0[-1]], point, a, b, v_val)
    return A, B, D
